# Functions
def parse_input():
    with open("input_05.txt", "r") as f:
        return [g.strip() for g in f]


# Start
lines = parse_input()
highestID = 0
ids = set()
for line in lines:
    # F is 0, B is 1
    # L is 0, R is 1
    line = line.replace("F", "0").replace("B", "1").replace("L", "0").replace("R", "1")
    highestID = max(highestID, int(line, 2))
    print(f"{line} : {int(line, 2)} : {highestID}")
    ids.add(int(line, 2))

print(f"Part 1 : {highestID}")

first = min(ids)
last = max(ids)
# OR also written missing = ids.symmetric_difference(set(range(first, last+1)))
# https://docs.python.org/3/library/stdtypes.html?highlight=set%20difference#frozenset.symmetric_difference
missing = ids ^ set(range(first, last + 1))
print(f"Part 2 : {missing}")
