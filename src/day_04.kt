import java.io.File

fun main() {
    val input = File("src/input_04.txt").readLines()

    var validPassports = 0

    val requiredFieldNames = arrayListOf(
            "byr",
            "iyr",
            "eyr",
            "hgt",
            "hcl",
            "ecl",
            "pid"
    )

    val currentPassportFields = mutableListOf<String>()
    for (line in input) {
        if (line == "") {
            // Empty line : check previous accumulated data and go to next passport
            if (currentPassportFields.containsAll(requiredFieldNames)) {
                validPassports++
            }
            currentPassportFields.clear()
            continue
        }

        val parts = line.split(" ")
        for (p in parts) {
            val keyValue = p.split(":")
            val k = keyValue[0]
            val v = keyValue[1]
            if (isValidField(k, v)) {
                currentPassportFields.add(k)
            }
        }
    }

    // Last line check
    if (currentPassportFields.containsAll(requiredFieldNames)) {
        validPassports++
    }

    println("Tests")
    println("Birth 12345 : ${isValidBirth("12345")}")
    println("Birth 1919 : ${isValidBirth("1919")}")
    println("Birth 1945 : ${isValidBirth("1945")}")
    println("Birth 2002 : ${isValidBirth("2002")}")

    println("Part 2 : $validPassports")
}


fun isValidField (fieldName : String, fieldValue : String) : Boolean {
    when (fieldName) {
        "byr" -> return isValidBirth(fieldValue)
        "iyr" -> return isValidIssue(fieldValue)
        "eyr" -> return isValidExpiration(fieldValue)
        "hgt" -> return isValidHeight(fieldValue)
        "hcl" -> return isValidHair(fieldValue)
        "ecl" -> return isValidEye(fieldValue)
        "pid" -> return isValidPassportId(fieldValue)
        "cid" -> return true
    }
    return false
}

fun isValidBirth(value: String) : Boolean {
    if (value.toInt() in 1920..2002) return true
    return false
}

fun isValidIssue(value: String) : Boolean {
    if (value.toInt() in 2010..2020) return true
    return false
}

fun isValidExpiration(value: String) : Boolean {
    if (value.toInt() in 2020..2030) return true
    return false
}

fun isValidHeight(value: String) : Boolean {
    if (value.length < 3) return false

    val suffix = value.substring(value.length-2 until value.length)
    val prefix = value.substring(0 until value.length-2)

    when (suffix) {
        "cm" -> if (prefix.toInt() in 150..193) return true
        "in" -> if (prefix.toInt() in 59..76) return true
    }
    return false
}

fun isValidHair(value: String) : Boolean {
    return value.matches(Regex("""#[0-9a-f]{6}"""))
}

fun isValidEye(value: String) : Boolean {
    return value in listOf("amb", "blu", "brn", "gry", "grn", "hzl", "oth")
}

fun isValidPassportId(value: String) : Boolean {
    return value.matches(Regex("""[0-9]{9}"""))
}