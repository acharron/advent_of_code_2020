
# Globals


# Functions
def parse_input():
    with open("input_08.txt", "r") as f:
        return [g.strip() for g in f]


def run_and_read_accumulator_on_finish(lines):
    accumulator = 0
    index = 0
    seen_indexes = set()
    safety = 0
    while safety < 100000:
        safety += 1
        if safety == 100000:
            return "end-of-while", accumulator

        if index == len(lines):
            return "last-index", accumulator

        if index in seen_indexes:
            return "infinite-loop", accumulator

        seen_indexes.add(index)

        op, arg = lines[index].split(" ")
        if op == "acc":
            sign, num = (arg[0], int(arg[1:]))
            if sign == "+":
                accumulator += num
            elif sign == "-":
                accumulator -= num
            index += 1
        elif op == "jmp":
            sign, num = (arg[0], int(arg[1:]))
            if sign == "+":
                index += num
            elif sign == "-":
                index -= num
            pass
        elif op == "nop":
            index += 1


# Main
def main():
    lines = parse_input()

    print(f"Part1 : {run_and_read_accumulator_on_finish(lines)}")

    for i in range(0, len(lines)):
        # Read line
        # If an acc, continue
        # If a jmp, turns into a nop and run
        # If a nop turns into a jmp and run
        op, arg = lines[i].split(" ")
        if op == "acc":
            continue
        elif op == "jmp":
            lines[i] = "nop" + " " + arg
            status, final_acc = run_and_read_accumulator_on_finish(lines)
            if status == "last-index":
                print(f"Part2 : {final_acc}")
                break
            else:
                # Revert the line to its original form
                lines[i] = "jmp" + " " + arg
        elif op == "nop":
            lines[i] = "jmp" + " " + arg
            status, final_acc = run_and_read_accumulator_on_finish(lines)
            if status == "last-index":
                print(f"Part2 : {final_acc}")
                break
            else:
                # Revert the line to its original form
                lines[i] = "nop" + " " + arg


# Start
main()
