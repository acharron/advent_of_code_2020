import java.io.File

val input = File("src/input_03.txt").readLines()
const val cEmpty = '.'
const val cTree  = '#'

fun main() {
    println("Part 1 = ${checkTree(3, 1)}")

    println("Part 2 : 1,1 = ${checkTree(1, 1)}")
    println("Part 2 : 3,1 = ${checkTree(3, 1)}")
    println("Part 2 : 5,1 = ${checkTree(5, 1)}")
    println("Part 2 : 7,1 = ${checkTree(7, 1)}")
    println("Part 2 : 1,2 = ${checkTree(1, 2)}")

    val res = checkTree(1, 1) *
            checkTree(3, 1) *
            checkTree(5, 1) *
            checkTree(7, 1) *
            checkTree(1, 2)

    println("Part 2 = $res")
}

fun checkTree(slopeX: Int, slopeY: Int): Int {
    var countTreesHit = 0
    var x = 0
    var y = 0

    while (y < input.size) {
        val currentLine = input[y]
        if (currentLine[x] == cTree) countTreesHit++

        // Go to next line
        y += slopeY
        x += slopeX
        if (x >= currentLine.length) x -= currentLine.length
    }

    return countTreesHit
}