def check_slope(row_list, slope_x, slope_y):
    x = 0
    y = 0
    res = 0

    while y < len(row_list) - 1:
        row = row_list[y]
        if x > len(row) - 1:
            x = x - len(row)
        if row[x] == "#":
            res += 1

        x += slope_x
        y += slope_y

    return res


with open("input_03.txt", "r") as f:
    # Same thing, but read takes everything at once, can lead to MemoryError?
    # inputs = f.read().splitlines()
    inputs = [line.strip() for line in f]

    result1 = check_slope(inputs, 3, 1)
    print(f"Part 1 : {result1}")

    a = check_slope(inputs, 1, 1)
    b = check_slope(inputs, 3, 1)
    c = check_slope(inputs, 5, 1)
    d = check_slope(inputs, 7, 1)
    e = check_slope(inputs, 1, 2)

    print(f"All five slopes : {a}  {b}  {c}  {d}  {e}")
    print(f"Part 2 : {a*b*c*d*e}")


