import java.io.File

/**
 * Had to re-read a bit about regular expressions for this one...
 *
 * I tried to have a complete regex at first, but it was easier to split first
 * and use a regex just to extract  number - color  of the contained bags
 *
 * Visualizing the problem with data class was really helpful, something to keep in mind
 *
 *
 * Help links:
 * https://docs.oracle.com/javase/8/docs/api/java/util/regex/Pattern.html
 * http://zetcode.com/kotlin/regularexpressions/
 * https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.collections/-mutable-set/add.html
 * https://chercher.tech/kotlin/tree-kotlin ?
 */


data class ContainerRule(val color: String, val number: Int, val inside: String = "")


val rules = mutableListOf<ContainerRule>()
const val objectiveColor = "shiny gold"

fun main() {
    val input = File("src/input_07.txt").readLines()

    //// Parsing ////
    val regex = Regex("""(\d+) (\w+ \w+)""")
    for (line in input) {
        val p = line.split(" bags contain ")
        val containerColor = p[0]
        val contents = p[1]

        if (contents == "no other bags.") {
            // Container doesn't need other bags inside
                rules.add(ContainerRule(containerColor, 0))
            continue
        }

        regex.findAll(contents).forEach { match ->
            val conditionNumber = match.groupValues[1].toInt()
            val conditionColor = match.groupValues[2]
            // Add conditions
            rules.add(ContainerRule(containerColor, conditionNumber, conditionColor))
        }
    }

    //// Search Part 1 ////
    val canContainShinyGold = mutableSetOf<String>()
    val colorsLeftToCheck = mutableListOf(objectiveColor)
    while (colorsLeftToCheck.isNotEmpty()) {
        val newContainers = rules.filter { it.inside == colorsLeftToCheck[0] }

        // Add to validated containers, in a set. If it's a new color, we have to check it
        newContainers.forEach {
            if (canContainShinyGold.add(it.color)) colorsLeftToCheck.add(it.color)
        }

        colorsLeftToCheck.removeAt(0)
    }

    println("Part 1 : ${canContainShinyGold.size}")


    //// Search Part 2 ////
    val cost = recursiveAddNumberBags(objectiveColor) - 1  // The question ask to not count the shiny gold bag itself
    println("Part 2 : $cost")
}

fun recursiveAddNumberBags(bag: String) : Int {
    return rules
            .filter { it.color == bag }  // Get all rules that the bag has to follow
            .sumBy {
                if (it.number == 0) return 1  // No child so it has a weight of 1
                else it.number * recursiveAddNumberBags(it.inside)
            } + 1
}


