import java.io.File

fun main() {
    val input = File("src/input_06.txt").readLines()

    var totalQuestions = 0
    var totalQuestions2 = 0
    val anyoneAnsweredQuestions = mutableSetOf<Char>()
    val everyoneAnsweredQuestions = mutableSetOf<Char>()
    var isFirstPersonInGroup = true

    for (line in input) {
        // Empty line : check previous accumulated data and go to next group
        if (line == "") {
            totalQuestions += anyoneAnsweredQuestions.size
            totalQuestions2 += everyoneAnsweredQuestions.size
            anyoneAnsweredQuestions.clear()
            everyoneAnsweredQuestions.clear()
            isFirstPersonInGroup = true
            continue
        }

        // Part 1
        line.toCharArray().toCollection(anyoneAnsweredQuestions)

        // Part 2
        if (isFirstPersonInGroup) {
            isFirstPersonInGroup = false
            line.toCharArray().toCollection(everyoneAnsweredQuestions)
        } else {
            val candidate = mutableSetOf<Char>()
            line.toCharArray().toCollection(candidate)

            everyoneAnsweredQuestions.removeIf { !candidate.contains(it) }
        }
    }
    // Last line
    totalQuestions += anyoneAnsweredQuestions.size
    totalQuestions2 += everyoneAnsweredQuestions.size


    println("Part 1 : $totalQuestions")
    println("Part 2 : $totalQuestions2")
}
