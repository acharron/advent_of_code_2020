# Functions
def parse_input():
    with open("input_06.txt", "r") as f:
        return [g.strip() for g in f]


def get_total_different_questions(group):
    questions = set()
    for person in group:
        questions = questions | set(person)
    return len(questions)


def get_total_same_questions(group):
    questions = set(group[0])
    for person in group:
        questions = questions & set(person)
    return len(questions)


# Start
lines = parse_input()

result1 = 0
result2 = 0
current_group = []
for i in range(0, len(lines)):
    if lines[i] != "":
        for a in lines[i].split(" "):
            current_group.append(a)

    if lines[i] == "" or i == len(lines) - 1:
        # ...
        result1 += get_total_different_questions(current_group)
        result2 += get_total_same_questions(current_group)
        current_group = []
        continue

print(f"Part1 : {result1}")
print(f"Part2 : {result2}")
