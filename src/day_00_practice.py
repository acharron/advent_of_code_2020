print("""\
Usage: thingy [OPTIONS]
     -h                        Display this usage message
     -H hostname               Hostname to connect to
""")

text = ("test aaa aaa"
        "bbbb")

a = [1, 2, 3]

b = a[:]

print(a)
print(b)

a[0] = 4

print(a)
print(b)

a = 0

print(a)
while a < 10:
    a += 1
    print(a)


def fib(n):  # write Fibonacci series up to n
    """Print a Fibonacci series up to n.
    
    :param n {int}"""
    i, j = 0, 1
    while i < n:
        print(i, end=' ')
        i, j = j, i + j
    print()


# Now call the function we just defined:
print(fib(2000))

print(fib.__doc__)



a = {1, 2, 3, 4, 5, 6, 7, 8, 9}
b = {1, 2, 3, 5, 6, 7, 8, 9}
print(a - b)

