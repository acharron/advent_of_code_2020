with open("input_02.txt", "r") as f:
    # Same thing
    # inputs = f.read().splitlines()
    inputs = [line.strip() for line in f]

    result1 = 0
    result2 = 0

    for i in inputs:
        parts = i.split(" ")
        first, second = [int(x) for x in parts[0].split("-")]
        target = parts[1].strip(":")
        password = parts[2]

        if first <= password.count(target) <= second:
            result1 += 1

        if (password[first-1] == target) ^ (password[second-1] == target):
            result2 += 1

    print(f"Part 1 : {result1}")
    print(f"Part 2 : {result2}")
