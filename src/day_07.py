import re


# Globals
pattern = r"([0-9]*) ([a-z ]+) bag[s]?"
parent_bags = {}
known_costs = {}


# Functions
def parse_input():
    with open("input_07.txt", "r") as f:
        return [g.strip() for g in f]


def get_outer_bags(to_find):
    res = []
    for p in parent_bags:
        for t in parent_bags[p]:
            if t[0] == to_find:
                res.append(p)
    return res


def cost(bag):
    if bag in known_costs:
        return known_costs[bag]

    list_contents = parent_bags[bag]
    if len(list_contents) == 0:
        known_costs[bag] = 1
        return 1
    buffer = 0
    for t in list_contents:
        buffer += t[1] * cost(t[0])
    known_costs[bag] = 1 + buffer
    return 1 + buffer


# Main
def main():
    lines = parse_input()
    # Making parent_bags
    # Dictionary of all the bags contained in a "parent" bag
    for line in lines:
        bag, content = line.split(" bags contain ")
        parent_bags[bag] = []
        if content == "no other bags.":
            continue
        for c in content.split(", "):
            number = int(re.search(pattern, c).group(1))
            color = re.search(pattern, c).group(2)
            parent_bags[bag].append((color, number))

    # Part 1
    # Bags containing directly shiny gold
    contains_shiny_gold = []
    outer_shiny_bag = get_outer_bags("shiny gold")
    for o in outer_shiny_bag:
        if o in contains_shiny_gold:
            continue
        contains_shiny_gold.append(o)

    # Plus all bags containing indirectly shiny gold
    for i in range(0, 10000):
        if i == len(contains_shiny_gold):
            break
        outer = get_outer_bags(contains_shiny_gold[i])
        for o in outer:
            if o in contains_shiny_gold:
                continue
            contains_shiny_gold.append(o)

    print(f"Part1 : {len(contains_shiny_gold)}")

    # Part 2
    # We only want to know the number of bags contained in the shiny gold
    # But we don't have to count the shiny gold in itself, so the result is - 1
    print(f"Part2 : {cost('shiny gold') - 1}")


# Start
main()

