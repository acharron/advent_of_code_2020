with open("input_01.txt", "r") as f:
    expenses = [int(line.strip("\n")) for line in f]

    print(expenses)

    for i in range(0, len(expenses)):
        for j in range(i, len(expenses)):

            if expenses[i] + expenses[j] == 2020:
                print(f"Part 1 : {expenses[i] * expenses[j]}")

            for k in range(j, len(expenses)):
                if expenses[i] + expenses[j] + expenses[k] == 2020:
                    print(f"Part 2 : {expenses[i] * expenses[j] * expenses[k]}")
