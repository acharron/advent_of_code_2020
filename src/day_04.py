import re

# Globals constants
target_fields = {"byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"}


# Functions
def read_passports():
    with open("input_04.txt", "r") as f:
        lines = [line.strip() for line in f]

        return lines


def is_valid_byr(value):
    return value.isnumeric() and int(value) in range(1920, 2003)


def is_valid_iry(value):
    return value.isnumeric() and int(value) in range(2010, 2021)


def is_valid_eyr(value):
    return value.isnumeric() and int(value) in range(2020, 2031)


def is_valid_hgt(value):
    pat = re.compile("^([0-9]+)(cm|in)$")
    if not pat.match(value):
        return False
    u, v = pat.match(value).groups()
    return (v == "cm" and int(u) in range(150, 194)) or (v == "in" and int(u) in range(59, 77))


def is_valid_hcl(value):
    return re.compile("^#[0-9a-f]{6}$").match(value)


def is_valid_ecl(value):
    return re.compile("^(amb|blu|brn|gry|grn|hzl|oth)$").match(value)


def is_valid_pid(value):
    return re.compile("^[0-9]{9}$").match(value)


def is_valid_field(name, value):
    if name == "byr":
        return is_valid_byr(value)
    elif name == "iyr":
        return is_valid_iry(value)
    elif name == "eyr":
        return is_valid_eyr(value)
    elif name == "hgt":
        return is_valid_hgt(value)
    elif name == "hcl":
        return is_valid_hcl(value)
    elif name == "ecl":
        return is_valid_ecl(value)
    elif name == "pid":
        return is_valid_pid(value)
    elif name == "cid":
        return True


def validate_passport(passport):
    print(passport)
    contained_fields = []
    for p in passport:
        field_name, field_value = p.split(":")
        if is_valid_field(field_name, field_value):
            contained_fields.append(field_name)

    # With an intersection, we check for all the required fields and ignore cid
    # {a, b, c, d} & {a, b, c} = {a, b, c}
    if set(contained_fields) & target_fields == target_fields:
        return True
    else:
        return False


# Start
inputs = read_passports()

result1 = 0
current_passport = []

for i in range(0, len(inputs)):
    if inputs[i] != "":
        for a in inputs[i].split(" "):
            current_passport.append(a)

    if inputs[i] == "" or i == len(inputs) - 1:
        if validate_passport(current_passport):
            result1 += 1
        current_passport = []
        continue

print(f"Part2 : {result1}")
