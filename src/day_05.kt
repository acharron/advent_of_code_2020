import java.io.File
import java.util.*
import kotlin.math.max

fun main() {
    val input = File("src/input_05.txt").readLines()

    val set = sortedSetOf<Int>()

    val bits = BitSet(10)
    var highestId = 0
    for (line in input) {
        bits.clear()
        for (i in line.indices) {
            when (line[i]) {
                'B', 'R' -> bits.set((line.length - 1) - i)
            }
        }
        highestId = max(highestId, bits.toLongArray()[0].toInt())
        println("$line : ${bits.toLongArray()[0]} : Highest = $highestId")

        set.add(bits.toLongArray()[0].toInt())
    }

    println("Part 1 : $highestId")

    // We get the 2 values before and after the missing one
    val missing = set.filterIndexed { index, it ->
        index != 0 &&           // Exclude the first and last IDs in the set : they of course don't have it-1 or it+1
        index != set.size-1 &&
        (!set.contains(it-1) || !set.contains(it+1))
    }

    println("Part 2 : ${missing.first()} - ${missing.last()}")
}
