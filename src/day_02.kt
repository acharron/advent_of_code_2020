import java.io.File

fun main() {
    val input = File("src/input_02.txt")
            .readLines()

    var numberValidPassword1 = 0
    var numberValidPassword2 = 0

    for (l in input) {
        val parts = l.split("-", " ", ": ")
        val min = parts[0].toInt()
        val max = parts[1].toInt()
        val condition = parts[2]
        val password = parts[3]

        val c = password.count { it == condition[0] }
        // (c <= min && c >= max) is replaceable by (c in min..max)
        if (c in min..max) numberValidPassword1++

        // The condition "Exactly one of these positions" is just a XOR operation
        // So we finish in one line
        if ((password[min-1] == condition[0]).xor(password[max-1] == condition[0])) numberValidPassword2++
    }

    println("Part 1 : $numberValidPassword1")
    println("Part 2 : $numberValidPassword2")
}