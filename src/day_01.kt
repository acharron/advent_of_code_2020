import java.io.File

fun main() {
    val input = File("src/input_01.txt")
        .readLines()
        .map { it.toInt() }


    for (i in 0 until input.size + 1) {
        for (j in i+1 until input.size) {
            if (input[i] + input[j] == 2020) {
                println("Part 1 = ${input[i] * input[j]}")
            }
            for (k in j+1 until input.size - 1) {
                if (input[i] + input[j] + input[k] == 2020) {
                    println("Part 2 = ${input[i] * input[j] * input[k]}")
                }
            }
        }
    }
}